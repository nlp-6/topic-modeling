import requests
from lxml import html
import json
from time import sleep




url_wiki = 'https://en.wikipedia.org/wiki/Wikipedia:Featured_articles'

response = requests.get(url_wiki)
#print(response)

parser = html.fromstring(response.text)
#print(parser)
articulos = parser.xpath("(//div[@class='hlist'])[2]/p/span/a/@href")
#print(articulos)
list = []
#count = 0
for ar in articulos:
    artdict = {}
    link = "https://en.wikipedia.org"+ar
    response2 = requests.get(link)
    parser = html.fromstring(response2.text)
    titulo = parser.xpath("//h1[@id='firstHeading']/text()")
    parrafo = parser.xpath("//div[@class='mw-parser-output']/p[2]/text()")
    artdict= {"link": link, "parrafo": ' '.join(parrafo).replace("\n","")}

    print(artdict)
    list.append(artdict)
    #count = count + 1
    #if(count > 25):
    #    break
    jsonString = json.dumps(list)
    f = open("../raw/wikidata.json","a")
    f.write(jsonString)
f.close()
