## tarea final GRUPO 6
- Honatan Palma
- Edgar García
- Jorge Ibarra



## pre requisitos
Tener instalas las siguientes librerias:

**cookiecutter**
!pip install cookiecutter

**pandas**
!pip install pandas

**sklearn**
!pip install sklearn

**spacy** 
!pip install -U spacy 

**pyLDAvis**
!pip install pyLDAvis

**Archivo JSON con los datos**


## Instrucciones
- Descargar el contenido general de topic_modeling

- dentro de topic_modeling se encuentran dos carpetas, notebooks y data
 
- dentro de data hay una carpeta que se llama raw donde se encuentra el wikidata.json que es el archivo a procesar.
 
- En la carpeta notebooks se encuentra wikirequest.py que es el scrappy que obtiene los articulos del wikipedia y que los guarda en la carpeta data\raw\wikidata.json
 
- En notebook tambien se encuentra TopicModeling donde se realizó el modelado de los datos.


# Recomendaciones
_En el archivo Readme final.docx cargamos el resumen y capturas de pantalla del proceso del topic modeling para un mejor entendimiento del proceso realizado._



